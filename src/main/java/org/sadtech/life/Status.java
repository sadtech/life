package org.sadtech.life;

public enum Status {
    BIRTH, LIFE, DEATH, CORPSE;

    public Status preparation(int numberLifeCell) {
        switch (this) {
            case CORPSE: return (numberLifeCell==3) ? BIRTH : CORPSE;
            case LIFE: return (numberLifeCell<2 || numberLifeCell>3) ? DEATH : LIFE;
            default: return this;
        }
    }

    public Status rebirth() {
        switch (this) {
            case BIRTH: return LIFE;
            case DEATH: return CORPSE;
            default: return this;
        }
    }
}
