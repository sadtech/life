package org.sadtech.life.gui;

import org.sadtech.life.Cell;
import org.sadtech.life.Status;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class Window implements Runnable {

    public static final int HEIGHT = 70;
    public static final int WIDTH = 70;

    JFrame frame;
    Box[][] boxes;

    @Override
    public void run() {
        initFrame();
        initBoxes();
        boxes[1][6].getCell().firstInit();
        boxes[1][7].getCell().firstInit();
        boxes[2][6].getCell().firstInit();
        boxes[2][7].getCell().firstInit();

        boxes[26][1].getCell().firstInit();
        boxes[26][2].getCell().firstInit();
        boxes[25][2].getCell().firstInit();
        boxes[24][2].getCell().firstInit();
        boxes[23][2].getCell().firstInit();
        boxes[25][3].getCell().firstInit();
        boxes[24][3].getCell().firstInit();
        boxes[23][3].getCell().firstInit();
        boxes[22][3].getCell().firstInit();
        boxes[22][4].getCell().firstInit();
        boxes[25][4].getCell().firstInit();
        boxes[25][5].getCell().firstInit();
        boxes[24][5].getCell().firstInit();
        boxes[23][5].getCell().firstInit();
        boxes[22][5].getCell().firstInit();
        boxes[26][6].getCell().firstInit();
        boxes[25][6].getCell().firstInit();
        boxes[24][6].getCell().firstInit();
        boxes[23][6].getCell().firstInit();
        boxes[26][7].getCell().firstInit();
        boxes[31][2].getCell().firstInit();
        boxes[31][3].getCell().firstInit();
        boxes[22][4].getCell().firstInit();
        boxes[35][4].getCell().firstInit();
        boxes[36][4].getCell().firstInit();
        boxes[35][5].getCell().firstInit();
        boxes[36][5].getCell().firstInit();

        boxes[12][5].getCell().firstInit();
        boxes[12][6].getCell().firstInit();
        boxes[12][7].getCell().firstInit();
        boxes[16][5].getCell().firstInit();
        boxes[16][6].getCell().firstInit();
        boxes[16][7].getCell().firstInit();
        boxes[17][5].getCell().firstInit();
        boxes[17][6].getCell().firstInit();
        boxes[17][7].getCell().firstInit();
        boxes[14][9].getCell().firstInit();
        boxes[15][8].getCell().firstInit();
        boxes[13][8].getCell().firstInit();
        boxes[14][3].getCell().firstInit();
        boxes[15][4].getCell().firstInit();
        boxes[13][4].getCell().firstInit();
        initTimer();
    }

    private void initTimer() {
        TimerListener t1 = new TimerListener();
        Timer timer = new Timer(50,t1);
        timer.start();
    }

    private void initBoxes() {
        boxes = new Box[WIDTH][HEIGHT];
        for (int i = 0; i < WIDTH; i++) {
            for (int j = 0; j < HEIGHT; j++) {
                boxes[i][j] = new Box(i, j);
                frame.add(boxes[i][j]);
            }
        }
    }

    private void print() {
        for (int i = 0; i < WIDTH; i++) {
            for (int j = 0; j < HEIGHT; j++) {
                boxes[i][j].setColor();
            }
        }
    }

    private void initFrame() {
        frame = new JFrame();
        frame.getContentPane().setLayout(null);
        frame.setSize(Box.SIZE * WIDTH, Box.SIZE * HEIGHT);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setLocationRelativeTo(null);
        frame.setVisible(true);
        frame.setTitle("Life Simulation");
    }

    private class TimerListener implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent e) {
            print();
            nextPopulationPreparation();
            print();
            nextPopulationRebirth();
        }
    }

    public void nextPopulationPreparation() {
        for (int i = 0; i < WIDTH; i++) {
            for (int j = 0; j < HEIGHT; j++) {
                boxes[i][j].getCell().preparation(livingAround(i,j));
            }
        }
    }

    public void nextPopulationRebirth() {
        for (int i = 0; i < WIDTH; i++) {
            for (int j = 0; j < HEIGHT; j++) {
                boxes[i][j].getCell().rebirth();
            }
        }
    }


    public int livingAround(int i, int j) {
        int count = 0;
        for (int k = i - 1; k < i + 2; k++) {
                for (int l = j - 1; l < j + 2; l++) {
                    if ((k != i || l != j)) {
                        Cell cell = boxes[(k+WIDTH) % WIDTH][(l+HEIGHT) % HEIGHT].getCell();
                        if (cell.getStatus() == Status.LIFE || cell.getStatus() == Status.DEATH) {
                            count++;
                        }
                    }
                }
        }
        return count;
    }

}
