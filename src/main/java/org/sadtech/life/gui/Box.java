package org.sadtech.life.gui;

import org.sadtech.life.Cell;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class Box extends JPanel {

    public static final int SIZE = 8;

    private Cell cell;

    public Cell getCell() {
        return cell;
    }

    public Box(int x, int y) {
        super();
        cell = new Cell();
        setBounds(x * SIZE, y * SIZE, SIZE, SIZE);
        setBackground(Color.BLACK);
        addMouseListener(new MouseAdapter() {
            @Override
            public void mousePressed(MouseEvent e) {
                cell.firstInit();
            }
        });
    }

    public void setColor() {
        switch (cell.getStatus()) {
            case BIRTH:
                setBackground(Color.BLACK);
                break;
            case LIFE:
                setBackground(Color.WHITE);
                break;
            case DEATH:
                setBackground(Color.GRAY);
                break;
            case CORPSE:
                setBackground(Color.BLACK);
                break;
        }
    }
}
