package org.sadtech.life;

import org.sadtech.life.gui.Window;

public class Life {

    public static void main(String[] args) {
        Life start = new Life();
        start.runLife();
    }

    public void runLife() {
        Window window = new Window();
        javax.swing.SwingUtilities.invokeLater(window);
    }
}
