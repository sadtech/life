package org.sadtech.life;

public class Cell  {

    private Status status;

    public Cell() {
        this.status = Status.CORPSE;
    }

    public void preparation(int numberLifeCell) {
        status = status.preparation(numberLifeCell);
    }

    public void rebirth() {
        status = status.rebirth();
    }

    public void firstInit() {
        status = status.LIFE;
    }

    public Status getStatus() {
        return status;
    }
}
